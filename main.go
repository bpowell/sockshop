package main

import (
	"fmt"

	"gitlab.com/bpowell/sockshop/pkg/catalogue"
	"gitlab.com/bpowell/sockshop/pkg/user"
)

const (
	user_svc      = "http://35.239.236.238"
	catalogue_svc = "http://35.223.87.105"
)

var (
	register_user  = fmt.Sprintf("%s/register", user_svc)
	update_card    = fmt.Sprintf("%s/cards", user_svc)
	list_catalogue = fmt.Sprintf("%s/catalogue", catalogue_svc)
)

func main() {
	if err := user.CreateUser(register_user); err != nil {
		fmt.Println("Problem testing /register endpoint")
		fmt.Println(err.Error())
	}

	fmt.Println()

	if err := user.UpdateCardInfo(update_card); err != nil {
		fmt.Println("Problem testing /cards endpoint")
		fmt.Println(err.Error())
	}

	fmt.Println()

	if err := catalogue.ListItemsFromCatalogue(list_catalogue); err != nil {
		fmt.Println("Problem testing /catalogue endpoint")
		fmt.Println(err.Error())
	} else {
		fmt.Println("/catalogue endpoint is working!")
	}
}
