package user

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

type user_form struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

type register_status_response struct {
	Id string `json:"id"`
}

var users []user_form

func createUserForms() error {
	contents, err := ioutil.ReadFile("users.json")
	if err != nil {
		return err
	}

	if err := json.Unmarshal(contents, &users); err != nil {
		return err
	}

	return nil
}

func CreateUser(url string) error {
	fmt.Println("Testing /register endpoint")
	if err := createUserForms(); err != nil {
		return err
	}

	for _, user := range users {
		payload, err := json.Marshal(user)
		if err != nil {
			return err
		}

		resp, err := http.Post(url, "application/json", bytes.NewBuffer(payload))
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			return errors.New(fmt.Sprintf("cannot create user, application gave status code of %d", resp.StatusCode))
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		var status register_status_response
		if err := json.Unmarshal(body, &status); err != nil {
			return err
		}

		if status.Id == "" {
			return errors.New(fmt.Sprintf("returned an invalid id when creating user %s", user.Username))
		}

		fmt.Printf("newly created user id for %s is %s\n", user.Username, status.Id)
	}

	fmt.Println("Testing complete for /register endpoint")
	return nil
}
