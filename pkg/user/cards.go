package user

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

type cardInfo struct {
	LongNum string `json:"longNum"`
	Expires string `json:"expires"`
	Ccv     string `json:"ccv"`
	UserId  string `json:"userId"`
}

var cards []cardInfo

func getCards() error {
	contents, err := ioutil.ReadFile("cards.json")
	if err != nil {
		return err
	}

	if err := json.Unmarshal(contents, &cards); err != nil {
		return err
	}

	return nil
}

func UpdateCardInfo(url string) error {
	fmt.Println("Testing POST /cards endpoint")
	if err := getCards(); err != nil {
		return err
	}

	for _, card := range cards {
		payload, err := json.Marshal(card)
		if err != nil {
			return err
		}

		resp, err := http.Post(url, "application/json", bytes.NewBuffer(payload))
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		status := struct {
			Id string `json:"id"`
		}{}
		if err := json.Unmarshal(body, &status); err != nil {
			return err
		}

		if status.Id == "" {
			return errors.New(fmt.Sprintf("returned an invalid id when create card for user %s\n", card.UserId))
		}

		fmt.Printf("newly created id for card added to %s is %s\n", card.UserId, status.Id)
	}

	fmt.Println("Testing complete for POST /cards")
	return nil
}
