package catalogue

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type catalog_item struct {
	Id          string   `json:"id"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	ImageUrl    []string `json:"imageUrl"`
	Price       float32  `json:"price"`
	Count       int      `json:count"`
	Tag         []string `json:"tag"`
}

var valid_items map[string]catalog_item

func createMapOfValidItems() error {
	contents, err := ioutil.ReadFile("catalogue.json")
	if err != nil {
		return err
	}

	var list_valid_items []catalog_item
	if err := json.Unmarshal(contents, &list_valid_items); err != nil {
		return err
	}

	valid_items = make(map[string]catalog_item)
	for _, i := range list_valid_items {
		valid_items[i.Id] = i
	}

	return nil
}

func ListItemsFromCatalogue(url string) error {
	fmt.Println("Testing /catalogue endpoint")
	if err := createMapOfValidItems(); err != nil {
		fmt.Println(err.Error())
		return err
	}

	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var items []catalog_item
	if err := json.Unmarshal(body, &items); err != nil {
		return err
	}

	problems := false
	for _, item := range items {
		if _, ok := valid_items[item.Id]; !ok {
			fmt.Printf("Item id %s is missing from the catalogue\n", item.Id)
			problems = true
		}
	}

	if !problems {
		fmt.Println("All items in the catalog have been found!")
	}

	fmt.Println("Testing complete")
	return nil
}
